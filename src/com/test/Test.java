package com.test;

import com.animals.*;
import com.collection.Zoo;

public class Test {
    public static void main(String[] args) {
        new Cat("Murzik").voice().walk();
        new Bird("Jack").voice().walk().fly();
        new Fish("Noname").voice().swim();

        Zoo<Creature> z = new Zoo<>(new Creature[]{
                new Cat("Cat 1"),
                new Cat("Cat 2"),
                new Bird("Bird 1"),
                new Bird("Bird 2"),
                new Fish("Fish")
        });
        z.print();
    }
}
