package com.animals;

public class Bird extends Creature implements Flyable, Walkable {
    public Bird(String name) {
        super(name);
    }

    @Override
    public Bird voice() {
        System.out.println("My name is " + getName() + ". Carr!");
        return this;
    }

    @Override
    public Bird fly() {
        System.out.println("I fly");
        return this;
    }

    @Override
    public Bird walk() {
        System.out.println("I walk");
        return this;
    }
}
