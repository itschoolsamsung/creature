package com.animals;

public class Cat extends Creature implements Walkable {
    public Cat(String name) {
        super(name);
    }

    @Override
    public Cat voice() {
        System.out.println("My name is " + getName() + ". Miu!");
        return this;
    }

    @Override
    public Cat walk() {
        System.out.println("I walk");
        return this;
    }
}
