package com.animals;

interface Walkable {
    Walkable walk();
}
