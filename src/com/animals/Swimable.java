package com.animals;

public interface Swimable {
    Swimable swim();
}
