package com.animals;

public abstract class Creature {
    private String name;

    abstract Creature voice();

    Creature(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
