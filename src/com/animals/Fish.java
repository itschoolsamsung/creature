package com.animals;

public class Fish extends Creature implements Swimable {
    public Fish(String name) {
        super(name);
    }

    @Override
    public Fish voice() {
        System.out.println("My name is " + getName() + ". I mute!");
        return this;
    }

    @Override
    public Fish swim() {
        System.out.println("I swim");
        return this;
    }
}
