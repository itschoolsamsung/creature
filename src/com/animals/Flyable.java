package com.animals;

interface Flyable {
    Flyable fly();
}
