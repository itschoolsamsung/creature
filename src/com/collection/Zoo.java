package com.collection;

public class Zoo<T> {
    private T[] animals;

    public Zoo(T[] animals) {
        setAnimals(animals);
    }

    public void setAnimals(T[] animals) {
        this.animals = animals;
    }

    public T[] getAnimals() {
        return animals;
    }

    public void print() {
        System.out.println("ZOO:");
        for (T animal : animals) {
            System.out.println(animal);
        }
    }
}
